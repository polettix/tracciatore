requires 'perl', '5.024000';
requires 'Crypt::Passphrase';
requires 'Crypt::Passphrase::Argon2';
requires 'Mojolicious';
requires 'Mojolicious::Plugin::Authentication';
requires 'Mojolicious::Plugin::Passphrase';
requires 'Mojo::Pg';
requires 'Mojo::SQLite';
requires 'IO::Socket::SSL';
requires 'Ouch';
requires 'Try::Catch';

on test => sub {
   requires 'Path::Tiny',      '0.084';
};

on develop => sub {
   requires 'Path::Tiny',          '0.084';
   requires 'Template::Perlish',   '1.52';
   requires 'Test::Pod::Coverage', '1.04';
   requires 'Test::Pod',           '1.51';
};
