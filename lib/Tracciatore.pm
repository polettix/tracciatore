package Tracciatore;
use v5.24;
use Mojo::Base 'Mojolicious', -signatures;
use Ouch ':trytiny_var';
use Try::Catch;
use Mojo::Util qw< b64_decode >;
use Tracciatore::Controller;
use Tracciatore::Model;

use constant MONIKER => 'tracciatore';
use constant DEFAULT_SECRETS => 'MTY4MTQ2ODk1Ni0wLjM0ODMzNzEyNjQ3MTEyOQ==';
use constant FRESHNESS_DELAY => 3; # seconds

has 'conf';
has model => \&_build_model;

sub startup ($self) {
   $self->moniker(MONIKER);
   $self->_startup_config          # must come first
      ->_startup_secrets
      ->_startup_model             # must come before authentication
      ->_startup_authentication
      ->_startup_hooks
      ->_startup_routes
      ;
   $self->controller_class('Tracciatore::Controller');
   $self->log->info('startup complete');
   return $self;
}

sub _startup_config ($self) {
   my $config = eval { $self->plugin('NotYAMLConfig') } || {};

   # prefixed with a moniker
   my $prefix = (uc(MONIKER) =~ s{-}{_}rgmxs) . '_';
   for my $key (qw<
         dsn
         remap_env
         skip_startup_db
      >) {
      my $env_key = $prefix . uc($key);
      $config->{$key} = $ENV{$env_key} if defined $ENV{$env_key};
   }

   # straight
   if (defined(my $straight = $config->{remap_env})) {
      for my $definition (split m{,}mxs, $straight) {
         my ($key, $env_key) = split m{=}mxs, $definition, 2;
         $config->{$key} = $ENV{$env_key} if defined $ENV{$env_key};
      }
   }

   $self->conf($config);
   return $self;
}

sub _build_model ($self) {
   my $dsn = $self->conf->{dsn} // ouch 500, 'no DSN set';
   return Tracciatore::Model->create($dsn);
}

sub __split_and_decode ($s) { map { b64_decode($_) } split m{\s+}mxs, $s }

sub _startup_secrets ($self) {
   my $config = $self->conf;
   my @secrets =
       defined $ENV{SECRETS}      ? __split_and_decode($ENV{SECRETS})
     : defined $config->{secrets} ? $config->{secrets}->@*
     :                              __split_and_decode(DEFAULT_SECRETS);
   $self->secrets(\@secrets);
   return $self;
} ## end sub _startup_secrets

sub _startup_model ($self) {
   $self->model->initialize;
   return $self;
}

sub _startup_authentication ($self) {
   my @validators;
   if (defined(my $validators = $self->conf->{password_validators})) {
      @validators = split(m{,}mxs, $validators =~ s{\s+}{}rgxms);
   }
   $self->plugin(
      Passphrase => {
         encoder => 'Argon2',
         (@validators ? (validators => \@validators) : ()),
      },
   );

   $self->plugin(
      Authentication => {
         load_user => sub ($c, $uid) {
            $self->model->get_account_by_id($uid);
         },
         validate_user => sub ($c, $username, $password, $extra) {
            my $account = $self->model->get_account_by_username($username)
               or return;
            return $account->{id}
               if $c->verify_password($password, $account->{password});
         }
      },
   );

   return $self;
}

sub _startup_hooks ($self) {
   $self->hook(
      after_dispatch => sub ($c) {
         $c->res->headers->cache_control('no-store, must-revalidate')
            if $c->is_user_authenticated;
      },
   );
   return $self;
}

sub _startup_routes ($self) {
   my $r = $self->routes->under('/')->to('auth#set_account');
   $r->any('/')->to('basic#root');
   $r->get('/config')->to('basic#config');
   $r->post('/login')->to('auth#login');
   $r->post('/logout')->to('auth#do_logout');
   $r->get('/logout')->to('auth#do_logout');

   my $private = $r->under('/private')->to('auth#check');
   $private->get('/foo')->to('basic#foo');

   $r->post('/api/logout')->to('auth#api_logout');
   $r->get('/api/logout')->to('auth#api_logout');
   $r->post('/api/login')->to('auth#api_login');

   my $record_api = $r->under('/api/record')->to('auth#api_check');

   # container-oriented endpoints
   $record_api->get('/')->to('user_api#get_records');
   $record_api->post('/')->to('user_api#add_record');

   # record-oriented endpoints
   $record_api->get('/:rid')->to('user_api#get_record');
   $record_api->put('/:rid')->to('user_api#put_record');
   $record_api->delete('/:rid')->to('user_api#del_record');

   return $self;
}

1;
