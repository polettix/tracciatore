=pod

=for vim
   vim: tw=72 ts=3 sts=3 sw=3 et ai :

=encoding utf8

=head1 NAME

Tracciatore - Trace data at one's pace


=head1 VERSION

This document describes Tracciatore version {{[ version ]}}.

=begin html

<a href="https://travis-ci.org/polettix/Tracciatore">
<img alt="Build Status" src="https://travis-ci.org/polettix/Tracciatore.svg?branch=master">
</a>
<a href="https://www.perl.org/">
<img alt="Perl Version" src="https://img.shields.io/badge/perl-5.24+-brightgreen.svg">
</a>
<a href="https://badge.fury.io/pl/Tracciatore">
<img alt="Current CPAN version" src="https://badge.fury.io/pl/Tracciatore.svg">
</a>
<a href="http://cpants.cpanauthors.org/dist/Tracciatore">
<img alt="Kwalitee" src="http://cpants.cpanauthors.org/dist/Tracciatore.png">
</a>
<a href="http://www.cpantesters.org/distro/O/Tracciatore.html?distmat=1">
<img alt="CPAN Testers" src="https://img.shields.io/badge/cpan-testers-blue.svg">
</a>
<a href="http://matrix.cpantesters.org/?dist=Tracciatore">
<img alt="CPAN Testers Matrix" src="https://img.shields.io/badge/matrix-@testers-blue.svg">
</a>

=end html

=head1 SYNOPSIS

   use Tracciatore;


=head1 DESCRIPTION

Yadda

=head1 INTERFACE

=head2 B<< foo >>

   my $foo = foo(qw< bar baz >);


=head1 BUGS AND LIMITATIONS

Minimul perl version 5.24.

Report bugs through GitHub (patches welcome) at
L<https://github.com/polettix/Tracciatore>.

=head1 AUTHOR

Flavio Poletti <flavio@polettix.it>

=head1 COPYRIGHT AND LICENSE

Copyright 2023 by Flavio Poletti <flavio@polettix.it>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


=cut
