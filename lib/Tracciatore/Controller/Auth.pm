package Tracciatore::Controller::Auth;
use Mojo::Base 'Tracciatore::Controller', -signatures;

sub set_account ($self) {
   my $acct = $self->is_user_authenticated ? $self->current_user : undef;
   $self->stash(account => $acct);
   return 1;
}

sub api_check ($self) {
   return 1 if $self->is_user_authenticated;
   $self->render(json => {status => 'error'}, status => 401);
   return 0;
}

sub api_logout ($self) {
   $self->logout;
   return $self->rendered(204);
}

sub api_login ($self) {
   my $username = $self->param('username');
   my $password = $self->param('password');
   return $self->rendered(204)
      if $self->authenticate($username, $password, {});
   return $self->render(json => {status => 'error'}, status => 401);
}

sub check ($self) {
   return 1 if $self->is_user_authenticated;
   $self->stash(account => undef); # safe side...
   $self->redirect_to('/');
   return 0;
}

sub do_logout ($self) {
   $self->logout;
   return $self->redirect_to('/');
}

sub login ($self) {
   my $username = $self->param('username');
   my $password = $self->param('password');
   return $self->redirect_to('/login')
      unless $self->authenticate($username, $password, {});
   return $self->redirect_to('/private/foo');
}

1;
