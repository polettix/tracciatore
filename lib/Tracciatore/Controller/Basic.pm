package Tracciatore::Controller::Basic;
use Mojo::Base 'Tracciatore::Controller', -signatures;

sub root ($self) {
   return $self->render(template => "open/index");
}

sub config ($self) {
   return $self->render(json => [ keys $self->app->conf->%* ]);
}

sub foo ($self) {
   return $self->render(template => 'private/foo');
}

1;
