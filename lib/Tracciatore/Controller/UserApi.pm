package Tracciatore::Controller::UserApi;
use Mojo::Base 'Tracciatore::Controller', -signatures;
use Try::Catch;
use Ouch ':trytiny_var';
use Scalar::Util 'blessed';

sub get_records ($self) {
   return $self->api_render(200 => []);
}

sub api_render ($self, $status, $data = undef) {
   return $self->render(
      status => $status,
      json => {
         status => $status,
         (defined($data) ? (data => $data) : ()),
      },
   );
}

sub wrapped_call ($self, $cb, $rethrow = 0) {
   my (undef, undef, undef, $subname) = caller(1);
   try { $cb->() }
   catch {
      my $e = $_;
      my $msg = $subname . ': ';
      my $status = 400;
      my $data = undef;
      if (blessed($e) && $e->isa('Ouch')) {
         $msg .= bleep();
         ($status, $data) = ($e->code, $e->data);
      }
      else {
         $msg .= $e;
      }
      $self->app->log->error($msg);
      die $_ if $rethrow;
      $self->api_render($status, $data);
   };
}

sub get_record ($self) {
   return $self->wrapped_call(
      sub {
         my $user = $self->current_user;
         my $rid = $self->param('rid');
         my $record = $self->model->get_record($user, $rid)
            or ouch 404, 'Not Found';
         return $self->api_render(200 => $record);
      }
   );
}

sub add_record ($self) {
   return $self->wrapped_call(
      sub {
         my $user = $self->current_user;
         my $record = $self->req->json;
         my $model = $self->model;
         my $rid = $model->add_record($user, $record)
            or ouch 400, 'Invalid input or other DB error';
         my $output_record = $model->get_record($user, $rid)
            or ouch 404, 'Not Found';
         return $self->api_render(200 => $output_record);
      }
   );
}

sub del_record ($self) {
   return $self->wrapped_call(
      sub {
         my $user = $self->current_user;
         my $rid = $self->param('rid');
         $self->model->delete_record($user, $rid);
         return $self->api_render(200);
      }
   );
}

1;
