package Tracciatore::Model;
use v5.24;
use Mojo::Base qw< -base -signatures >;
use Ouch ':trytiny_var';
use Try::Catch;
use Mojo::JSON 'encode_json';

has 'dsn';
has 'mojodb' => sub ($self) { $self->_build_mojodb };

use constant ID_FIELD => 'user_eid';

# alias to "new()" for sub-classes, dispatch if here in this base class
sub create ($package, $dsn, @rest) {
   return $package->new(dsn => $dsn, @rest) if $package ne __PACKAGE__;

   my $technology = $dsn =~ m{postgres}imxs ? 'Pg' : 'SQLite';
   my $model_class = join '::', __PACKAGE__, $technology;
   (my $packpath = $model_class . '.pm') =~ s{::}{/}gmxs;
   require $packpath;
   return $model_class->new(dsn => $dsn, @rest);
}

sub initialize ($self) {
   $self->ensure_schema;
   return $self;
}

# Abstract base class interface to be implemented by sub-classes
sub _build_mojodb { ouch 500, '_build_mojodb(): abstract method' }
sub migrations { ouch 500, 'migrations(): abstract method' }

sub ensure_schema ($self) {
   $self->mojodb->migrations->from_string($self->migrations)->migrate;
   return $self;
}

sub get_account ($self, %where) {
   my $db = $self->mojodb->db;
   my $rs = $db->select(account => undef, \%where);
   my $hashes = $rs->hashes;
   $rs->finish;
   return if $hashes->size != 1;
   my $account = $hashes->first;
   $account->{id} = $account->{ID_FIELD()};
   return $account;
}

sub get_account_by_username ($self, $username) {
   $self->get_account(username => $username);
}

sub get_account_by_id ($self, $id) {
   $self->get_account(ID_FIELD() => $id);
}

sub _prepare_record_data ($self, $user, $record) {
   my %insertable = map { 'data_' . $_ => $record->{$_} }
      qw< time key value  >;
   $insertable{data_meta} = encode_json($record->{meta} // {});
   $insertable{user_id} = $user->{user_id}; # foreign key in database
   $insertable{recording_time} = time();
   $insertable{data_time} //= $insertable{recording_time};
   my @tags = ($record->{tags} // [])->@*;
   return (\%insertable, \@tags);
}

sub add_record ($self, $user, $record) { ... }

sub get_record ($self, $user, $rid) {
   $user = $user->{user_id} if ref($user);
   my $db = $self->mojodb->db;
   my $record;
   try {
      my $tx = $db->begin;

      my $results = $db->select(
         record => undef, { record_id => $rid, user_id => $user },
      );
      my $hashes = $results->hashes;
      $results->finish;

      if ($hashes->size) {
         $record = $hashes->first;
         my $results = $db->select(tag => undef, { record_id => $rid });
         $record->{tags} = [ $results->hashes->each ];
         $results->finish;
      }

      $tx->commit;
   }
   catch {
      warn $_;
   };
   return $record;
}

sub delete_record ($self, $user, $rid) {
   $user = $user->{user_id} if ref($user);
   my $db = $self->mojodb->db;
   eval {
      my $tx = $db->begin;

      # FIXME this should be addressed differently...
      my $rs = $db->select(record => record_id => { record_id => $rid });
      my $count = $rs->hashes->size;
      $rs->finish;

      if ($count > 0) {
         $db->delete(tag => { record_id => $rid });
         $db->delete(record => { record_id => $rid, user_id => $user });
      }

      $tx->commit;
   } or die $@;
   return $rid;
}

1;
