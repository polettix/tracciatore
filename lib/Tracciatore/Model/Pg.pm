package Tracciatore::Model::Pg;
use Mojo::Base 'Tracciatore::Model', -signatures;

sub _build_mojodb ($self) {
   require Mojo::Pg;
   return Mojo::Pg->new($self->dsn);
}

sub migrations ($self) {
   return <<'END';
-- 1 up
CREATE TABLE IF NOT EXISTS account (
   user_id     SERIAL PRIMARY KEY,
   user_eid    VARCHAR(32) UNIQUE NOT NULL,
   username    VARCHAR(32) UNIQUE NOT NULL,
   password    VARCHAR(255) NOT NULL,
   email       VARCHAR(255) UNIQUE NOT NULL,
   created_on  TIMESTAMP NOT NULL,
   last_access TIMESTAMP
);

CREATE TABLE IF NOT EXISTS record (
   record_id      SERIAL PRIMARY KEY,
   user_id        INT NOT NULL,
   data_time      TIMESTAMP NOT NULL,
   data_key       VARCHAR(255) NOT NULL,
   data_value     NUMERIC,
   data_meta      TEXT,
   recording_time TIMESTAMP NOT NULL,
   FOREIGN KEY (user_id) REFERENCES account (user_id)
);

CREATE TABLE IF NOT EXISTS tag (
   tag_id    SERIAL PRIMARY KEY,
   record_id INT NOT NULL,
   value     VARCHAR(50) NOT NULL,
   FOREIGN KEY (record_id) REFERENCES record (record_id)
);

-- 1 down
DROP TABLE tag;
DROP TABLE record;
DROP TABLE account;
END
}

sub add_record ($self, $user, $record) {
   my ($data, $tags) = $self->_prepare_record_data($user, $record);
   my $rid;
   my $db = $self->mojodb->db;
   eval {
      my $tx = $db->begin;

      my $rs = $db->insert(record => $data, {returning => 'record_id'});
      my $hashes = $rs->hashes;
      $rs->finish;
      $rid = $hashes->first->{record_id};

      $db->insert(tag => { value => $_, record_id => $rid }) for $tags->@*;

      $tx->commit;
   };
   return $rid;
}

1;
