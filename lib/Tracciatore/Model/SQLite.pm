package Tracciatore::Model::SQLite;
use Mojo::Base 'Tracciatore::Model', -signatures;
use Try::Catch;

sub _build_mojodb ($self) {
   require Mojo::SQLite;
   return Mojo::SQLite->new($self->dsn);
}

sub migrations ($self) {
   return <<'END';
-- 1 up
CREATE TABLE IF NOT EXISTS account (
   user_id     INTEGER PRIMARY KEY,
   user_eid    VARCHAR(32) UNIQUE NOT NULL,
   username    VARCHAR(32) UNIQUE NOT NULL,
   password    VARCHAR(255) NOT NULL,
   email       VARCHAR(255) UNIQUE NOT NULL,
   created_on  INTEGER NOT NULL,
   last_access INTEGER
);

CREATE TABLE IF NOT EXISTS record (
   record_id      INTEGER PRIMARY KEY,
   user_id        INT NOT NULL,
   data_time      INTEGER NOT NULL,
   data_key       VARCHAR(255) NOT NULL,
   data_value     NUMERIC,
   data_meta      TEXT,
   recording_time INTEGER NOT NULL,
   FOREIGN KEY (user_id) REFERENCES account (user_id)
);

CREATE TABLE IF NOT EXISTS tag (
   tag_id    INTEGER PRIMARY KEY,
   record_id INT NOT NULL,
   value     VARCHAR(50) NOT NULL,
   FOREIGN KEY (record_id) REFERENCES record (record_id)
);

-- 1 down
DROP TABLE tag;
DROP TABLE record;
DROP TABLE account;
END
}

sub add_record ($self, $user, $record) {
   my ($data, $tags) = $self->_prepare_record_data($user, $record);
   my $db = $self->mojodb->db;
   my $rid;
   try {
      my $tx = $db->begin;
      $rid = $db->insert(record => $data)->last_insert_id;
      $db->insert(tag => { value => $_, record_id => $rid }) for $tags->@*;
      $tx->commit;
   }
   catch { die $_ };
   return $rid;
}

1;
